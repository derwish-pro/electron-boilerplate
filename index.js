var side = false;
$("#settings-btn").click(function () {
    side = !side;
    if (!side)
        showSettings();
    else
        hideSettings();
});

function showSettings() {
    $(".sidebar").css("left", "-150px");
    $(".main").css("left", "0");
}

function hideSettings() {
    $(".sidebar").css("left", "0px");
    $(".main").css("left", "150px");
}